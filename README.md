# Projeto de Segurança Informática

O objetivo deste trabalho consiste em desenvolver um sistema que permita distribuir aplicações de forma segura, garantindo que apenas são executadas pelos donos legítimos das mesmas.
O princípio básico consiste na criação de uma licença que não é mais do que uma assinatura criada pelo autor, de alguma forma relacionada com a aplicação em execução.

## Este sistema deverá ser composto por:
- um conjunto de ferramentas que executam nas instalações do autor da aplicação;
- uma biblioteca de controlo de execução que é incorporada nas aplicações a proteger.

Deve ser protegida uma qualquer aplicação desenvolvida numa qualquer linguagem de programação, que neste caso será Java.
As ferramentas disponibilizadas ao autor devem permitir criar ficheiros de licenças que são fornecidos às aplicações, e que devem incluir uma especificação de execução e a identificação dos dados da licença. 
A biblioteca de controlo de execução deverá ser constituída por um módulo contendo um conjunto de funcionalidades. É importante que a biblioteca seja completamente independente da aplicação desenvolvida.

## O processo será dividido em duas grandes partes:

###	Pedido de licença (registo)
- o processo de registo implica a biblioteca recolher toda a informação necessária (utilizador, sistema e aplicação), este é expresso num documento, (pedido de registo [JSON]). 

- o documento deve ser assinado pela chave presente no Cartão de Cidadão do utilizador em causa, sendo depois	o documento de pedido de registo deve ser guardado num ficheiro para ser enviado para o autor da aplicação
###	Validação/Emissão da licença
- o primeiro passo consiste em validar a assinatura da mesma e decifrar o seu conteúdo
- cada aplicação distribuída contém uma chave que fornece à biblioteca de proteção aquando da sua inicialização
- a biblioteca deve ser capaz de tolerar pequenas alterações ao sistema
- a biblioteca, fazendo uso da chave pública do utilizador registada na licença poderá autenticar o utilizador
- a duração de cada licença é definida pelo autor
- a informação relativa à identificação da aplicação serve para validar a integridade do sistema


###	Fluxo do sistema

![alt text](<./system_flow.jpg>) 


###	Fluxo de validação

![alt text](<./validation_flow.jpg>) 
